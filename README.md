# GitHub Stars

A simple SPA (Single Page App) for search and starring GitHub repositories
according to the login  of the user.

# Introduction

I was going to use [Next.js](https://github.com/zeit/next.js/) initially, but due some issues with its SSR and unique way to get initial props, I decided to use vanilla React - specifically the [`create-react-app`](https://github.com/facebook/create-react-app) boilerplate from Facebook.

## Challenges

First and foremost, learn about GitHub GraphQL API, its queries, mutations and all the quirks. But the most time consuming challenge was querying from an *external* API. To this day, the APIs I've consumed were self hosted (i.e same host, another port). I've used the [`react-apollo`](https://github.com/apollographql/react-apollo) package, since it's the one I'm used to. Unfortunately the new release lacks a decent documentation and a lot of stuff I've done was all about trial and error (what better way to learn, right? :))

## Motivations and approaches

I tried to use the latest on both React and CSS/HTML. For instance, instead of ES6 Classes, all but one component are Functional Components, which are [faster](https://twitter.com/trueadm/status/916706152976707584) and easier to create a better [*separation of concerns*](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0). There's a lot of articles regarding this issue, but I like [this one](https://medium.com/@learnreact/container-components-c0e67432e005) and is the way I've been using so far. This section would be incomplete without mentioning the [`recompose`](https://github.com/acdlite/recompose) package, which makes handling of states, handlers and much more *so* much easier.

All of the above combined with the use of [HOCs](https://reactjs.org/docs/higher-order-components.html), really makes React a joy to code.

As for HTML and CSS, I've tried to use the latest and where possible semantic way to create the components. Here's some articles I found relevant:

+ [We Write CSS Like We Did in the 90s, and Yes, It’s Silly](https://alistapart.com/article/we-write-css-like-we-did-in-the-90s-and-yes-its-silly)
+ [MDN CSS grid](https://developer.mozilla.org/en-US/docs/Web/CSS/grid)

---

## Using the application

Since this repository is based on `create-react-app` its better to follow the instructions from themselves. But the basics:

1. Clone the repository:
```
$ git clone https://gitlab.com/trinaldi/xerpa.git
```

2. Go to the newly created folder
```
$ cd path/to/folder
```
3. `yarn` install the packages
```
$ yarn install
```
4. Test drive it
```
$ yarn start
```

Below some guides from `create-react-app`
+ [Available Scripts section](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#available-scripts)
+ [Deploy section](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#deployment)


---
### Possible Caveats

I had to create an OAuth app from GitHub and the credentials are all set to `http://localhost:3000`. Also, I've also created a Gatekeeper Heroku dyno to communicate with GitHub's API. There may be some caveats with this process. GitHub doesn't let anyone use the queries and mutations for this project, so I had to create a Login step before. Kudos for [this article](https://www.graphql.college/building-a-github-client-with-react-apollo/#github-authentication) for the tip. I used the same technique in order to get the Authorization.

---
## Relevant literature

+ [Why The Hipsters Recompose Everything](https://medium.com/javascript-inside/why-the-hipsters-recompose-everything-23ac08748198)
+ [Building HOCs with Recompose](https://medium.com/front-end-developers/building-hocs-with-recompose-7debb951d101)
+ [Top 5 Recompose HOCs](https://medium.com/@abhiaiyer/top-5-recompose-hocs-1a4c9cc4566)
+ [Simplify your React components with Apollo and Recompose](https://blog.apollographql.com/simplify-your-react-components-with-apollo-and-recompose-8b9e302dea51)
+ [Full-stack React + GraphQL Tutorial](https://blog.apollographql.com/full-stack-react-graphql-tutorial-582ac8d24e3b)

## Feedback wanted!
