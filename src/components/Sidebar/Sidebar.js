import React, { Fragment } from "react";
import "./Sidebar.css";
import RepositoriesList from "../RepositoryList/RepositoryList" 


const Sidebar = ({
  props
}) => (
  <Fragment>
  <section className="sidebar-container">
      <div className="sidebar-top-container">
        <img className="sidebar-avatar" 
          src={props.avatarUrl}
          alt={props.name} />
        <p>{props.name}</p>
        <p>{props.login}</p>
      </div>
      <div className="sidebar-bottom-container">
        <p>{props.bio}</p>
        <p><i className="material-icons md-18">people_outline</i>{props.following.nodes.map(e => `@${e.name}`).join(" ")}</p>
        <p><i className="material-icons md-18">location_on</i>{props.location}</p>
        <p><i className="material-icons md-18">email</i>{props.email}</p>
        <p><i className="material-icons md-18">public</i>{props.websiteUrl}</p>
      </div>

    </section>
      {props.repositories
      && props.repositories.nodes
      && (<RepositoriesList props={props} />)}
    </Fragment>
  );

  export default Sidebar