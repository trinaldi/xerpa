import SearchRepos from "./SearchRepos";
import { withState, compose } from "recompose";
import { graphql } from "react-apollo";
import GET_REPOS_QUERY from "../../gql/GetRepos";

const enhance = compose(
  withState('firstSearch', 'setFirstSearch', false),
  withState('keyword', 'setKeyword', ''),
  graphql(GET_REPOS_QUERY, {
    options: (props) => ({
      variables: { username: props.keyword }
    }),
    props: ({ data: { error, loading, user } }) => {
      return {
        data: user ? user : [],
        loading,
        error
      };
    }
  }),
  
  
)

export default enhance(SearchRepos)