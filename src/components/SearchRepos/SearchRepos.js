import React, {Fragment} from "react";
import SearchReposTop from "../SearchReposTop";

import { Formik, Field, Form } from 'formik'
import "./SearchRepos.css";

const SearchRepos = ({
  setFirstSearch,
  firstSearch,
  setKeyword,
  error,
  data,
  }) => (
      <Formik
      initialValues={{
        keyword: ''
      }}
      onSubmit={(props) => {
        setKeyword(props.keyword);
        setFirstSearch(true);
      }}
      render={() => (
        <Fragment>
          <div className="center-container"
            style={{display: !firstSearch ? "flex" : "none"}}>
        <Form>
          <section className="initial">
          <h1>Github<span>Stars</span></h1>
          <Field
            name="keyword" 
            placeholder="GitHub username..."
            className="search"
            />
          </section>
        </Form>
          </div>
          {data.avatarUrl && <SearchReposTop props={data}/>}
          {firstSearch && error && <SearchReposTop props={data}/>}
      </Fragment>
      )}
/>
);

export default SearchRepos