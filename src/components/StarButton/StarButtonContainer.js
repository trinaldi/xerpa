import { compose, withState, withHandlers } from 'recompose';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import StarButton from "./StarButton";

gql`
query GetRepos($username: String!){
	user(login: $username) {
    repositories(first: 4) {
      nodes {
        owner {
          login
        }
        id
        nameWithOwner
        description
        viewerHasStarred
        stargazers {
          totalCount
        }
      }
    }
  }
}
`;

const ADD_STAR = gql`
mutation AddStar($starrableId: ID!) {
  addStar(input: {starrableId: $starrableId}) {
    starrable {
      id
      viewerHasStarred
    }
  }
}
`;

const REMOVE_STAR = gql`
  mutation RemoveStar($starrableId: ID!) {
    removeStar(input: { starrableId: $starrableId }) {
      starrable {
        id
        viewerHasStarred
      }
    }
  }
`;

const enhance = compose(
  withState("success", "setSuccess", {}),
  graphql(ADD_STAR, {
    name: "doAddStar",
    options: {refetchQueries: [ 'GetRepos']}
  }),
  graphql(REMOVE_STAR, {
    name: "doRemoveStar",
    options: {refetchQueries: [ 'GetRepos']}
  }),
  withHandlers({
    addStar: ({
      setSuccess,
      doAddStar,
      id
    }) => () => {
      return doAddStar({
        variables: {
          starrableId: id
        }
      }).then(({data}) => {
        setSuccess(data.addStar.starrable.viewerHasStarred)
      })
    },
    removeStar: ({
      setSuccess,
      doRemoveStar,
      id
    }) => () => {
      return doRemoveStar({
        variables: {
          starrableId: id
        }
      }).then(({data}) => {
        setSuccess(data.removeStar.starrable.viewerHasStarred)
      })
    }
  })
)

export default enhance(StarButton)