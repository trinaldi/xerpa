import React from 'react';
import "./StarButton.css";

const StarButton = ({
  addStar,
  removeStar,
  starred,
  username,

}) => (
    <button 
      className={starred ? "unstar-button" : "star-button"}
      username={username}
      onClick={starred ? removeStar : addStar}>
      
      {starred ? "unstar" : "star"}

    </button>  
);

export default StarButton