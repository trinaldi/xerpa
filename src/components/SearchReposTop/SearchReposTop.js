import React, {Fragment} from "react";
import { Avatar } from "gitstar-components";
import { Formik, Field, Form } from 'formik'
import Sidebar from "../Sidebar"
import NoResults from "../NoResults"
import "./SearchReposTop.css";

const SearchReposTop = ({
  setFirstSearch,
  firstSearch,
  setKeyword,
  error,
  data,
  props
  }) => (
    <Formik
    initialValues={{
      keyword: ''
    }}
    onSubmit={(props) => {
      setKeyword(props.keyword)
      setFirstSearch(true);
    }}
    render={() => (
      <Fragment>
          <section className="after-search">
          <a href="/">Github<span>Stars</span></a>
          <Form>
            <Field
              name="keyword" 
              placeholder="GitHub username..."
              className="search"
              />
            </Form>
          <Avatar className="top-avatar" />
          </section>
          {!error && data.name && <Sidebar props={data || ''}/>}
          {error && (
            <NoResults />
          )}
      </Fragment>
      )}
/>
);

export default SearchReposTop