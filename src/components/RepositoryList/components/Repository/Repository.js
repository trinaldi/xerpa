import React from "react";
import StarButton from "../../../StarButton"
import "./Repository.css";

const Repository = ({
  props
}) => (
  <div className="repository-container">
    <p>{props.nameWithOwner}</p>
    <p>{props.description}</p>
    <p><i className="material-icons">star_border</i>{props.stargazers.totalCount}</p>
    <StarButton id={props.id} username={props.login} starred={props.viewerHasStarred}/>
  </div>
)

export default Repository