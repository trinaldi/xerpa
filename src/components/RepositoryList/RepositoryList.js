import React from "react";
import Repository from "./components/Repository/Repository"
import "./RepositoryList.css";

const RepositoryList = ({
  props
}) => (
    <section className="repositories-dialog">
      {props.repositories.nodes
      .filter(r => r.owner.login === props.login)
      .map(r => {
        return (
          <Repository key={r.id} props={r} />
        )
      })
      }
    </section>
  );

  export default RepositoryList