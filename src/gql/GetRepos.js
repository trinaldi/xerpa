import gql from "graphql-tag";

const GET_REPOS_QUERY = gql`
query GetRepos($username: String!){
	user(login: $username) {
    avatarUrl
    name
    login
    bio
    location
    email
    websiteUrl
    following(first: 4) {
			nodes {
        name
      }
    }
    repositories(first: 4) {
      nodes {
        owner {
          login
        }
        id
        nameWithOwner
        description
        viewerHasStarred
        stargazers {
          totalCount
        }
      }
    }
  }
}
`
export default GET_REPOS_QUERY