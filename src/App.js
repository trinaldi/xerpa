import React, { Component } from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

import SearchRepos from "./components/SearchRepos/"

import "./App.css";

import {
  STATUS,
  Container,
} from "gitstar-components";

const CLIENT_ID = "399d1bb9b2e9b21d3d09";
const REDIRECT_URI = "http://localhost:3000/";
const AUTH_API_URI = "https://trinaldi-xerpa.herokuapp.com/authenticate/";

const client = new ApolloClient({
  uri: "https://api.github.com/graphql",
  request: operation => {
    const token = localStorage.getItem("github_token");
    if (token) {
      operation.setContext({
        headers: {
          authorization: `Bearer ${token}`
        }
      });
    }
  }
});

class App extends Component {
  state = {
    status: STATUS.INITIAL
  };
  componentDidMount() {
    const storedToken = localStorage.getItem("github_token");
    if (storedToken) {
      this.setState({
        status: STATUS.AUTHENTICATED
      });
      return;
    }
    const code =
      window.location.href.match(/\?code=(.*)/) &&
      window.location.href.match(/\?code=(.*)/)[1];
    if (code) {
      this.setState({ status: STATUS.LOADING });
      fetch(`${AUTH_API_URI}${code}`)
        .then(response => response.json())
        .then(({ token }) => {
          if (token) {
            localStorage.setItem("github_token", token);
          }
          this.setState({
            status: STATUS.FINISHED_LOADING
          });
        });
    }
  }

  render() {
    return (
      
      <ApolloProvider client={client}>
      <Container>
        
        {this.state.status !== STATUS.INITIAL &&
        <SearchRepos />
        }
        <div className={this.state.status === STATUS.INITIAL ? "login" : ""}>
        <a
        className="login-button"
        style={{ 
            display: this.state.status === STATUS.INITIAL ? "inline" : "none"
            }}
            href={`https://github.com/login/oauth/authorize?client_id=${CLIENT_ID}&scope=user%20public_repo%20gist&redirect_uri=${REDIRECT_URI}`}
          >
            {
              this.state.status === STATUS.INITIAL ? "Login" : "Logado!"
            }
          </a>
          </div>
      
      

      </Container>
      </ApolloProvider>
      
    );
  }
}

export default App;